#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>
#include <stdbool.h>        

#define NUM_PIEZAS 32

//Estructura para representar una pieza
struct Pieza{
    char nombre;
    int valor;
    char columna;
    int fila;
    char color;
}; 

struct Pieza piezas[NUM_PIEZAS]; //Variable global, continene las piezas del juego

bool validarMovimiento(char columna, int fila);
char ubicarPieza(int fila, char columna);
void imprimirTablero(); 
void inicializar(); //Inicializa las piezas
void analizarEntrada(char entrada[]);
void mover(struct Pieza *p, char columna, int fila);
struct Pieza* encontrarPieza(int fila, char columna);

int 
main (){
    //Creación de expresión regular para validar entrada (POSIX)
    regex_t regex;
    if (regcomp(&regex, "^[a-h][0-8]([[:space:]]|[x]|[=])[a-h][0-8][+]?$", REG_EXTENDED)) 
        printf("Error al compilar exp. regular");
    
    inicializar(piezas);
    imprimirTablero(piezas);
    char entrada[6]; //Tamaño máximo de entrada (basado en notación de ajedrez) == 5
    for (;;){   //Main loop
        fgets(entrada, 6,stdin);
        getchar();  //Come salto de línea
        if(regexec(&regex, entrada, 0, NULL, 0)){ //Validación de entrada
            printf("Formato inválido\n");
            continue;
        }
        analizarEntrada(entrada);
    }
}

void analizarEntrada(char entrada[]){
  if(validarMovimiento(entrada[0], entrada[1]-'0')){
    struct Pieza *piezT = encontrarPieza(entrada[0], entrada[1]-'0');  
    switch(entrada[2]){
        case '=': break; //Promoción
        case 'x': case ' ': 
        break; //Movimiento básico (" "), captura("x")
    }
  }  
}

void mover(struct Pieza *p, char columna, int fila){ 
  //Aquí
}

bool validarMovimiento(char columna, int fila){
    for (int i = 0; i < NUM_PIEZAS; i++){
        if (piezas[i].fila == fila && piezas[i].columna == columna && piezas[i].color == 'b')   return true;
        else if(piezas[i].fila == fila && piezas[i].columna == columna){
            printf("No puedes mover esa pieza\n");
            return false;
        }
    }
    printf("No hay niguna pieza ahí\n");
    return false;   
}

void imprimirTablero(){ //8x8
    printf(" a b c d e f g h\n\n"); //Filas
    for (int i = 8; i > 0; i--){
        printf("[%c,%c,%c,%c,%c,%c,%c,%c] %d\n",
        ubicarPieza(i,'a'),ubicarPieza(i,'b'), ubicarPieza(i,'c'), ubicarPieza(i,'d'),
        ubicarPieza(i,'e'),ubicarPieza(i,'f'),ubicarPieza(i,'g'),ubicarPieza(i,'h'),i);
    }
}

char ubicarPieza(int fila, char columna){ //Función que busca pieza en la coordenada dada y regresa el char pertinente 
    for (int i = 0; i < NUM_PIEZAS; i++)
        if (piezas[i].fila == fila && piezas[i].columna == columna) return piezas[i].nombre;
    return ' ';
}

struct Pieza* encontrarPieza(int fila, char columna){
  for (int i = 0; i < NUM_PIEZAS; i++)
        if (piezas[i].fila == fila && piezas[i].columna == columna) return &piezas[i];
}

void inicializar(){ //Función para crear las 32 piezas del juego
    //Peones, 16 piezas
    for(int i = 0; i < 8; i++){
        //Blancos
        piezas[i].nombre = 'P';
        piezas[i].valor = 10;
        piezas[i].columna = 97+i;
        piezas[i].fila = 2;
        piezas[i].color = 'b';

        //Negros
        piezas[i+8].nombre = 'p';
        piezas[i+8].valor = 10;
        piezas[i+8].columna = 97+i;
        piezas[i+8].fila = 7;
        piezas[i+8].color = 'n';
    }

    //Torres; 4 espacios
    for(int i = 16; i < 18; i++){
        piezas[i].nombre = 'T';
        piezas[i].valor = 50;
        piezas[i].columna = i < 17 ? 'a':'h';
        piezas[i].fila = 1;
        piezas[i].color = 'b';

        piezas[i+2].nombre = 't';
        piezas[i+2].valor = 50;
        piezas[i+2].columna = i < 17 ? 'a':'h';
        piezas[i+2].fila = 8;
        piezas[i+2].color = 'n';
    }

    //Caballos; 4 espacios
    for(int i = 20; i < 22; i++){
        piezas[i].nombre = 'C';
        piezas[i].valor = 30;
        piezas[i].columna = i < 21 ? 'b' : 'g';
        piezas[i].fila = 1;
        piezas[i].color = 'b';

        piezas[i+2].nombre = 'c';
        piezas[i+2].valor = 30;
        piezas[i+2].columna = i < 21 ? 'b' : 'g';
        piezas[i+2].fila = 8;
        piezas[i+2].color = 'n';
    }

    //Alfil; 4 espacios
    for(int i = 24; i < 26; i++){
        piezas[i].nombre = 'A';
        piezas[i].valor = 30;
        piezas[i].columna = i < 25 ? 'c' : 'f';
        piezas[i].fila = 1;
        piezas[i].color = 'b';

        piezas[i+2].nombre = 'a';
        piezas[i+2].valor = 30;
        piezas[i+2].columna = i < 25 ? 'c' : 'f';
        piezas[i+2].fila = 8;
        piezas[i+2].color = 'n';

    }

    //Queen y reyes
    piezas[28].nombre = 'Q';
    piezas[28].valor = 90;
    piezas[28].columna = 'd';
    piezas[28].fila = 1;
    piezas[28].color = 'b';
    
    piezas[29].nombre = 'q';
    piezas[29].valor = 90;
    piezas[29].columna = 'd';
    piezas[29].fila = 8;
    piezas[29].color = 'n';

    piezas[30].nombre = 'K';
    piezas[30].valor = 900;
    piezas[30].columna = 'e';
    piezas[30].fila = 1;
    piezas[30].color = 'b';

    piezas[31].nombre = 'k';
    piezas[31].valor = 900;
    piezas[31].columna = 'e';
    piezas[31].fila = 8;
    piezas[31].color = 'n';
}
